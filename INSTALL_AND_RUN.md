## If YOU JUST WANT TO USE IT:

#### find the IP of your providers dns server:
- go online with your vpn
- press the windows-key and type CMD and a window opens 
- type in "ipconfig /all" and find the line which says 'DNS-Server'
- there should be an adress with 4 numbers with 3 dots in between
- take this to the next step

#### you want to configure the dns server of your provider in
bin\config.ini
on the line where it says 'ip = ' overwrite the adress with yours

#### execute  
bin\always_on_indicator.exe
on the command line or just double click

## If YOU WANT TO TAKE THE HARD WAY AND WANT TO RUN THE PYTHON SCRIPT:
installation of the libs and execution for windows 10:  
  
Install Python 3.7 from:  
[www.python.org](https://www.python.org/downloads/ "python.org downloads")  
check that it is working with:   
`python -V`  
should give you something like or greater than:  
Python 3.7.x

open an adminstrative shell like [cmder](https://cmder.net/ "cmder"), cmd, powershell and   
check that the python package manager is working with  
`pip -V`   
if you have no pip or its not working:  
check [pip installation](https://pip.pypa.io/en/stable/installing/ "pip installation") 

now we need the python windows api library with pip:  
`pip install pywin32`  

debugging pip(cygwin):  
if you can not connect to the internet because of your corporate proxy   
you need to set   
`export http_proxy=http://username:pass@yourproxy:portnumber`  
`export https_proxy=http://username:pass@yourproxy:portnumber`  
note: at the time of this writing the  
command line proxy parameters of pip DO NOT WORK(which took a while to figure out)

finally running it:  
go - in the shell of your liking - to the directory  
.   
and do  
`python always_on_indicator.py`  

if all goes as planned:  
In your taskbar should appear an new icon (an Autobahn sign at the moment).   
On mouseover you should see your a text with your status "status: online" or "status: offline". 
Aditionally the number of checks made which were "ok" or "not ok" will be shown.   
This essentially means that the script could connect to the nameserver configured in less that a second. If the Autobahn sign has a 
red line through it you are probably offline or your connection is really really really slow at the moment.

COMPILING AN EXE I USED:  
 [Auto Python To Exe](https://pypi.org/project/auto-py-to-exe/ "Auto Python To Exe")




